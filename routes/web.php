<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('logout', 'Backend\Access\User\UsersController@logout')->name('users.logout');
Route::patch('/admin/access/users/password/{user}', 'Backend\Access\User\UsersController@password');
Route::resource('admin/access/roles', 'Backend\Access\Role\\RolesController');
Route::resource('admin/access/users', 'Backend\Access\User\\UsersController');
Route::resource('admin/access/permissions', 'Backend\Access\Permission\\PermissionsController');
Route::resource('admin/access/permissions', 'Backend\Access\Permission\\PermissionsController');
Route::resource('admin/access/roles', 'Backend\Access\Role\\RolesController');