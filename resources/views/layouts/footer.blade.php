<footer class="main-footer">
    <strong>Copyright &copy; {{ date('Y') }} <a href="http://labospartcons.org/" target="_blank">LaboSpart Consulting</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>+(237)690756614/+(237)650673702</b> 
    </div>
  </footer>