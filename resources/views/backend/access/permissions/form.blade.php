<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}} row">
<div class="col-md-4">
	{!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
</div>
    <div class="col-md-8">
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('label') ? 'has-error' : ''}} row">
<div class="col-md-4">
	{!! Form::label('label', 'Label', ['class' => 'control-label']) !!}
</div>
    <div class="col-md-8">
    {!! Form::text('label', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('label', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
    <button type="button" class="btn btn-danger float-right" data-dismiss="modal">Close</button>
</div>
