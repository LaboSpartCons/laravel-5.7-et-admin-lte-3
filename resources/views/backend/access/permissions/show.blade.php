<div class="modal fade" id="myModalView-{{ $item->id }}">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="card">
                  <div class="card-header no-border bg-info-gradient">
                    <h3 class="card-title">
                      <center>Show Permission</center>
                    </h3>
                    <div class="card-tools">
                      <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                  <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <th>ID</th><td>{{ $item->id }}</td>
                                        </tr>
                                        <tr><th> Name </th><td> {{ $item->name }} </td></tr><tr><th> Label </th><td> {{ $item->label }} </td></tr>
                                    </tbody>
                                </table>
                            </div>
<button type="button" class="btn btn-danger float-right" data-dismiss="modal">Close</button>
                        </div>
                    </div>
        </div>
    </div>
</div>
