@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card">
            <div class="card-header no-border bg-info-gradient">
                    <h3 class="card-title">
                      <center>Roles</center>
                    </h3>
                    <!-- card tools -->
                    <div class="card-tools">
                        <button type="button"
                              class="btn btn-info btn-sm"
                              data-widget="collapse"
                              data-toggle="tooltip"
                              title="Collapse">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.card-tools -->
              </div>
              <div class="card-body">
                    <!-- Button to Open the Modal -->
                        <button type="button" class="btn btn-lg btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i>
                          Add New Role
                        </button>
                        @include('backend/access.roles.create')
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table id="example1" class="table table-hover">
                                <thead class="bg-dark">
                                    <tr>
                                        <th>#</th><th>Name</th><th>Label</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($roles as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td><td>{{ $item->label }}</td>
                                        <td>
                                            <button type="button" class="btn btn-lg btn-info btn-sm" data-toggle="modal" data-target="#myModalView-{{ $item->id }}" title="Show Role"><i class="fa fa-eye" aria-hidden="true"></i>
                                              View
                                            </button>
                                            @include('backend/access.roles.show')
                                            <button type="button" class="btn btn-lg btn-primary btn-sm" data-toggle="modal" data-target="#myModalEdit-{{ $item->id }}" title="Edit Role"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                              Edit
                                            </button>
                                            @include('backend/access.roles.edit')
                                            <button type="button" class="btn btn-lg btn-danger btn-sm" data-toggle="modal" data-target="#myModalDelete-{{ $item->id }}" title="Delete Role"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                              Delete
                                            </button>
<div class="modal fade" id="myModalDelete-{{ $item->id }}">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="card">
                  <div class="card-header no-border bg-info-gradient">
                    <h3 class="card-title">
                      <center>Delete Role</center>
                    </h3>
                    <div class="card-tools">
                      <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                  <h3>Voulez vous vraiment supprimez?</h3>
                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/access/roles', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                            <button type="button" class="btn btn-primary float-right" data-dismiss="modal">Close</button>
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger float-left',
                                                        'title' => 'Delete Role'
                                                )) !!}
                                            {!! Form::close() !!}
                        </div>
                    </div>
        </div>
    </div>
</div>

                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $roles->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
