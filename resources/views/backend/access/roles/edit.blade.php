<div class="modal fade" id="myModalEdit-{{ $item->id }}">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
        <div class="card">
              <div class="card-header no-border bg-info-gradient">
                <h3 class="card-title">
                  <center>Edit Role</center>
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($item, [
                            'method' => 'PATCH',
                            'url' => ['/admin/access/roles', $item->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('backend/access.roles.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
