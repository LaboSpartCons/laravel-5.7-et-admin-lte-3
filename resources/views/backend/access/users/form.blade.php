<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}} row">
<div class="col-md-4">
	{!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
</div>
    <div class="col-md-8">
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('prenom') ? 'has-error' : ''}} row">
<div class="col-md-4">
	{!! Form::label('prenom', 'Prenom', ['class' => 'control-label']) !!}
</div>
    <div class="col-md-8">
    {!! Form::text('prenom', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('prenom', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}} row">
<div class="col-md-4">
	{!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
</div>
    <div class="col-md-8">
    {!! Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(!isset($item))
    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}} row">
        <div class="col-md-4">
            {!! Form::label('password', 'Password', ['class' => 'control-label']) !!}
        </div>
        <div class="col-md-8">
            {!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
<div class="form-group row">
    <div class="col-md-4">
            {!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'control-label']) !!}
        </div>
        <div class="col-md-8">
            {!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>
@else
    <div class="form-group {{ $errors->has('role_id') ? 'has-error' : ''}} row">
        <div class="col-md-4">
            {!! Form::label('role_id', 'Role', ['class' => 'control-label']) !!}
        </div>
        <div class="col-md-8">
        {{-- Form::select('role_id', array_pluck($roles, 'label', 'id'), $item->roles, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Sélectionnez le Rôle']) --}}
            @forelse($roles as $role)
                <label>
                    {!! Form::checkbox('role_id[]', $role->id, $role->users->contains($item), ['class' => 'flat-red']) !!}
                    {{ $role->label }}
                </label>
                @if($loop->last)
                @else
                    <br>
                @endif
            @empty
            <p>Aucun Rôle Enregistrer pour le moment</p>
            @endforelse
        {!! $errors->first('role_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('avatar') ? 'has-error' : ''}} row">
        <div class="col-md-4">
            {!! Form::label('avatar', 'Avatar', ['class' => 'control-label']) !!}
        </div>
        <div class="col-md-8">
                <input type="file" accept="image/*" onchange="loadFile(event)" name="avatar" id="avatar" class="form-control">
                <img id="output" class="img-fluid" src="{{ asset('images/avatar/'.$item->avatar) }}" />
                {!! $errors->first('avatar', '<p class="help-block">:message</p>') !!}
        </div>
        <!-- Affichage de l'image!-->
        <script>
          var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };
        </script>
    </div>
@endif


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary', 'id'=>'submit-all']) !!}
    <button type="button" class="btn btn-danger float-right" data-dismiss="modal">Fermer</button>
</div>

  
