@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card">
            <div class="card-header no-border bg-info-gradient">
                    <h3 class="card-title">
                      <center>Users</center>
                    </h3>
                    <!-- card tools -->
                    <div class="card-tools">
                        <button type="button"
                              class="btn btn-info btn-sm"
                              data-widget="collapse"
                              data-toggle="tooltip"
                              title="Collapse">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.card-tools -->
              </div>
              <div class="card-body">
                    <!-- Button to Open the Modal -->
                        <button type="button" class="btn btn-lg btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i>
                          Add New User
                        </button>
                        @include('backend/access.users.create')
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table id="example1" class="table table-hover">
                                <thead class="bg-dark">
                                    <tr>
                                        <th>#</th><th>Name</th><th>Prenom</th><th>Email</th><th>Roles</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($users as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td><td>{{ $item->prenom }}</td><td>{{ $item->email }}</td><td>
                                            @forelse($item->roles as $role)
                                                {{ $role->label }}
                                                @if(!$loop->last)
                                                {!! ',<br>' !!}
                                                @endif
                                            @empty
                                            <p>Aucun Rôle</p>
                                            @endforelse</td>
                                        <td>
                                            <button type="button" class="btn btn-lg btn-info btn-sm" data-toggle="modal" data-target="#myModalView-{{ $item->id }}" title="Show User"><i class="fa fa-eye" aria-hidden="true"></i>
                                              View
                                            </button>
                                            @include('backend/access.users.show')
                                            <button type="button" class="btn btn-lg btn-primary btn-sm" data-toggle="modal" data-target="#myModalEdit-{{ $item->id }}" title="Edit User"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                              Edit
                                            </button>
                                            @include('backend/access.users.password')
                                            <button type="button" class="btn btn-lg btn-dark btn-sm" data-toggle="modal" data-target="#myModalPass-{{ $item->id }}" title="Edit User Password"><i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                            </button>
                                            @include('backend/access.users.edit')
                                            <button type="button" class="btn btn-lg btn-danger btn-sm" data-toggle="modal" data-target="#myModalDelete-{{ $item->id }}" title="Delete User"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                              Delete
                                            </button>
<div class="modal fade" id="myModalDelete-{{ $item->id }}">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="card">
                  <div class="card-header no-border bg-info-gradient">
                    <h3 class="card-title">
                      <center>Delete User</center>
                    </h3>
                    <div class="card-tools">
                      <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                    <h3>Voulez vous Vraiment supprimer?</h3>
                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/access/users', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                            <button type="button" class="btn btn-primary float-right" data-dismiss="modal">Close</button>
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger  float-left',
                                                        'title' => 'Delete User'
                                                )) !!}
                                            {!! Form::close() !!}
                        </div>
                    </div>
        </div>
    </div>
</div>

                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $users->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
