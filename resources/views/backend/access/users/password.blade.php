<div class="modal fade" id="myModalPass-{{ $item->id }}">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
        <div class="card">
              <div class="card-header no-border bg-info-gradient">
                <h3 class="card-title">
                  <center>Edit User Password</center>
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body box-profile">
                  <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{ asset('images/avatar/'.$item->avatar) }}"
                       alt="{{ $item->name.' '.$item->prenom }}">
                </div>
                <h3 class="profile-username text-center">{{ $item->name.' '.$item->prenom }}</h3>
                <p class="text-muted text-center">
                @forelse($item->roles as $role)
                  {{ $role->label }}
                  @if(!$loop->last)
                  {!! ',<br>' !!}
                  @endif
                @empty
                </p>
                <p>Aucun Rôle</p>
                @endforelse
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        {!! Form::model($item, [
                            'method' => 'PATCH',
                            'url' => ['/admin/access/users/password', $item->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}} row">
                          <div class="col-md-4">
                              {!! Form::label('password', 'Password', ['class' => 'control-label']) !!}
                          </div>
                          <div class="col-md-8">
                              {!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
                          {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-4">
                                  {!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'control-label']) !!}
                              </div>
                              <div class="col-md-8">
                                  {!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required']) !!}
                              </div>
                        </div>
                        <div class="form-group">
    {!! Form::submit('Update Password', ['class' => 'btn btn-dark', 'id'=>'submit-all']) !!}
    <button type="button" class="btn btn-danger float-right" data-dismiss="modal">Fermer</button>
</div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
