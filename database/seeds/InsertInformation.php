<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
class InsertInformation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::connection()->getDriverName() == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        if (DB::connection()->getDriverName() == 'mysql') {
            DB::table('permission_role')->truncate();
            DB::table('role_user')->truncate();
            DB::table('users')->truncate();
            DB::table('roles')->truncate();
            DB::table('permissions')->truncate();
        } elseif (DB::connection()->getDriverName() == 'sqlite') {
            DB::statement('DELETE FROM permission_role');
            DB::statement('DELETE FROM role_user');
            DB::statement('DELETE FROM users');
            DB::statement('DELETE FROM roles');
            DB::statement('DELETE FROM permissions');
            DB::statement('UPDATE sqlite_sequence SET seq = 0 where name ="roles"');
        } else {
            //For PostgreSQL or anything else
            DB::statement('TRUNCATE TABLE permission_role CASCADE');
            DB::statement('TRUNCATE TABLE role_user CASCADE');
            DB::statement('TRUNCATE TABLE users CASCADE');
            DB::statement('TRUNCATE TABLE roles CASCADE');
            DB::statement('TRUNCATE TABLE permissions CASCADE');
        }

        //Add the master administrator, user id of 1
        $users = [
            [
                'name'              => 'Admin Admin',
                'prenom'            => 'administrateur',
                'email'             => 'admin@admin.com',
                'password'          => Hash::make('Password1'),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'User',
                'prenom'            => 'BackEnd User',
                'email'             => 'user@admin.com',
                'password'          => Hash::make('Password1'),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Executive',
                'prenom'            => 'Front End User',
                'email'             => 'user@user.com',
                'password'          => Hash::make('Password1'),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
        ];
        $roles = [
            [
                'name'       => 'admin',
                'label'      =>'Administrateur',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
             [
                'name'       => 'director',
                'label'      =>'Directeur Général',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
             [
                'name'       => 'manager',
                'label'      =>'Gestionnaire',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            
        ];
        $permissions = [
            [
                'name'       => 'administration',
                'label'      =>'Superviseur Général',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'visuelle',
                'label'      =>'Visualisation totale',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'gestion',
                'label'      =>'Gestion des données',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];
        $role_user= [
                ['role_id'=>1, 'user_id'=>1],
                ['role_id'=>2, 'user_id'=>2],
                ['role_id'=>3, 'user_id'=>3],
            ];
        DB::table('users')->insert($users);
        DB::table('roles')->insert($roles);
        DB::table('permissions')->insert($permissions);
        DB::table('role_user')->insert($role_user);

        if (DB::connection()->getDriverName() == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
