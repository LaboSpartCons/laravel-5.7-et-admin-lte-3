<?php
use Illuminate\Database\Seeder;
use App\Models\Access\Permission\Permission;
use App\Models\Access\Role\Role;
use Carbon\Carbon;
class AccessControlList extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {/*
        $permissions = [
        	[
                'name'       => 'configuration-users',
                'display_name'        => 'Configuration et enregistrement des données de base',
                'sort'       => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'Home-Parcour-more',
                'display_name'        => "Plus d'information sur le parcour",
                'sort'       => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'Home-Institution-more',
                'display_name'        => "Plus d'information sur les institutions membre",
                'sort'       => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'Home-User-more',
                'display_name'        => "Plus d'information sur les Utilisateurs",
                'sort'       => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'Home-Suggestions',
                'display_name'        => "Affichage des Suggestions de Home Page",
                'sort'       => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'IM-manager',
                'display_name'        => "Ajout d'une nouvelle Institution Membre",
                'sort'       => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'COLLECTE-DATA',
                'display_name'        => "Gestion des parcours de formations",
                'sort'       => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'FORUM',
                'display_name'        => "Forum de Communication des experts",
                'sort'       => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            
        ];
        Permission::insert($permissions);
        // ******************* CONTROLE D'ACCES ET DONNEES DE BASE ************************
        $permission=Permission::where('name','configuration-users')->first();
        $permission->attachRoles([1]);
        // *************************************************************

        // ***************** HOME PAGE *********************************
        // *************************************************************
        $permission=Permission::where('name','Home-Parcour-more')->first();
        $permission->attachRoles([1, 2, 3]);
        // **************************************************
        $permission=Permission::where('name','Home-Institution-more')->first();
        $permission->attachRoles([1, 2, 3]);
        // **************************************************
        $permission=Permission::where('name','Home-User-more')->first();
        $permission->attachRoles([1]);
        // **************************************************
        $permission=Permission::where('name','Home-Suggestions')->first();
        $permission->attachRoles([1, 2]);
        // **************** END HOME PAGE ******************************
        // ************* Insitution Membre *****************************
        $permission=Permission::where('name','IM-manager')->first();
        $permission->attachRoles([1]);
      	// **************************** END IM **************************
      	// ************** COLLECTE-DATA **********************************
      	$permission=Permission::where('name','COLLECTE-DATA')->first();
        $permission->attachRoles([1, 3]);
        // *************** END COLLECTE DATA ****************************
        // ************** FORUM **********************************
      	$permission=Permission::where('name','FORUM')->first();
        $permission->attachRoles([1, 2]);
        // *************** END FORUM ****************************
      */
    }
}

