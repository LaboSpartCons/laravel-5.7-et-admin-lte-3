<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Auth, Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Access\User\User;
use App\Models\Access\Role\Role;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $users = User::where('name', 'LIKE', "%$keyword%")
                ->orWhere('prenom', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('password', 'LIKE', "%$keyword%")
                ->orWhere('avatar', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $users = User::latest()->paginate($perPage);
        }
        $roles=Role::all();

        return view('backend/access.users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $data)
    {
        
        Validator::make($data->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'prenom' => 'required|string|max:255',
        ])->validate();
        
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'prenom'=>$data['prenom'],
            'password' => Hash::make($data['password']),
        ]);

        return redirect('admin/access/users')->with('flash_message', 'User added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('backend/access.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('backend/access.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        Validator::make($request->all(), [
            'name'=>'required',
            'prenom'=>'required',
            'email'=>'required',
            'avatar'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'role_id'=>'required',
        ])->validate();
         if(!empty($request->avatar))
        {
            $user->roles()->detach(Role::all());
            $avatar=substr($request->avatar->getClientOriginalName(), 0,-4);
            $fileName = $avatar.time().'.'.$request->avatar->getClientOriginalExtension();
            $request->avatar->move('images/avatar', $fileName);
            $user->update(['name'=>$request->name, 'prenom'=>$request->prenom, 'email'=>$request->email, 'avatar'=>$fileName]);
            $user->roles()->attach($request->role_id);
        }
        else
        {
            $user->roles()->detach(Role::all());
            $user->update(['name'=>$request->name, 'prenom'=>$request->prenom, 'email'=>$request->email]);
            $user->roles()->attach($request->role_id);
        }
        return redirect('admin/access/users')->with('update', 'User updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect('admin/access/users')->with('delete', 'User deleted successfully!');
    }
    public function password(Request $request, $id)
    {
        $user = User::findOrFail($id);
        Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ])->validate();
        $user->update(['password'=>Hash::make($request->password)]);
        return redirect('admin/access/users')->with('flash_message', 'User Password updated successfully!');
    }
    public function logout(Request $request)
    {
        auth()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
}
