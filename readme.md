<p align="center">
<a href="http://labospartcons.org/"><img src="http://labospartcons.org/wp-content/uploads/2018/10/cropped-logo-labosparCons1.png"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.
## After download execute
- composer install
- php artisan migrate
- php artisan db:seed

## Generate Crud Generate

 php artisan crud:generate Roles --fields="name#string; prenom#string; email#string; password#string; avatar#string" --view-path=backend/access --controller-namespace=Backend\\Access\\Role --route-group=admin/access --model-namespace=Models\\Access\\Role --form-helper=laravelcollective

## Update the file show.blade.php
replace the name of model by $item

